package com.mitrich.game

data class User(
    var totalNumberOfCoins: Int,
    var lastTimePresent: Long,
    val listOfCreatures: ArrayList<Creature>
)

var user = User(30, 0L, listOfAllCreatures)