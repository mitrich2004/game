package com.mitrich.game

import android.annotation.SuppressLint
import android.util.Log
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.floor

@SuppressLint("SdCardPath")
const val userDataFileName = "userData.json"

@SuppressLint("SdCardPath")
const val filesDir = "/data/user/0/com.mitrich.game/files"

fun convertSecondsToHours(numberOfSeconds: Int): Int {
    return floor(numberOfSeconds.toDouble() / 3600).toInt()
}

fun countTimeToNextGenerationInMinutes(
    timeToNextGenerationInSeconds: Int,
    numberOfHoursToNextGeneration: Int
): Int {
    return floor((timeToNextGenerationInSeconds.toDouble() - numberOfHoursToNextGeneration * 3600) / 60).toInt()
}

fun countTimeToNextGenerationInSeconds(
    generationTime: Long,
    timeSincePreviousGeneration: Long
): Int {
    return (generationTime / 1000 - timeSincePreviousGeneration / 1000).toInt()
}

fun countNumberOfGenerationsMissed(
    timeSincePreviousGeneration: Long,
    timeUserWasAbsent: Long,
    generationTime: Long
): Int {
    return floor(((timeSincePreviousGeneration + timeUserWasAbsent) / generationTime).toDouble()).toInt()
}

fun countTimeSincePreviousGeneration(
    timeSincePreviousGeneration: Long,
    timeUserWasAbsent: Long,
    generationTime: Long,
    numberOfGenerationsMissed: Int
): Long {
    return timeSincePreviousGeneration + timeUserWasAbsent - (generationTime * numberOfGenerationsMissed)
}

fun formatNumber(number: Int): String {
    val numberString = when {
        number.toDouble() / 1000000 >= 1 -> {
            val numberOfMillions = (number.toDouble() / 1000000).toString()
            var roundedNumberOfMillions =
                numberOfMillions.substring(0, numberOfMillions.indexOf('.') + 2)
            if (roundedNumberOfMillions[roundedNumberOfMillions.length - 1] == '0') {
                roundedNumberOfMillions =
                    roundedNumberOfMillions.substring(0, roundedNumberOfMillions.indexOf('.'))
            }
            "${roundedNumberOfMillions}M"
        }
        number.toDouble() / 1000 >= 1 -> {
            val numberOfThousands = (number.toDouble() / 1000).toString()
            var roundedNumberOfThousands =
                numberOfThousands.substring(0, numberOfThousands.indexOf('.') + 2)
            if (roundedNumberOfThousands[roundedNumberOfThousands.length - 1] == '0') {
                roundedNumberOfThousands =
                    roundedNumberOfThousands.substring(0, roundedNumberOfThousands.indexOf('.'))
            }
            "${roundedNumberOfThousands}K"
        }
        else -> {
            number.toString()
        }
    }
    return numberString
}