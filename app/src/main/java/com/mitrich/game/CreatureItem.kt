package com.mitrich.game

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.creature_dialog.*
import kotlinx.android.synthetic.main.creature_item_layout.view.*
import java.io.File
import java.util.*

class CreatureItem(
    val creature: Creature,
    private val totalNumberOfCoinsTextView: TextView,
    private val mainActivityContext: Context
) : Item<ViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.creature_item_layout
    }

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {

        if (!creature.isUnlocked) {
            totalNumberOfCoinsTextView.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    stylizeUnlockButton(viewHolder)
                }

                override fun afterTextChanged(s: Editable?) {}
            })
        }

        val creatureHandler = Handler(Looper.getMainLooper())
        var creatureHandlerRunner: Runnable? = null

        if (creature.isUnlocked) {
            countCoinsGeneratedWhileUserWasAbsent()
            viewHolder.itemView.number_of_coins.text =
                "${formatNumber(creature.currentNumberOfCoins)}/${formatNumber(creature.maxNumberOfCoins)}"
            if (creature.level < 5) {
                viewHolder.itemView.creature_image.setImageResource(creature.firstLevelImageId)
            } else {
                viewHolder.itemView.creature_image.setImageResource(creature.fifthLevelImageId)
            }
            stylizeCollectButton(viewHolder)
        } else {
            stylizeLockedCreature(viewHolder)
        }

        creatureHandler.post(object : Runnable {
            override fun run() {
                if (creatureHandlerRunner == null) {
                    creatureHandlerRunner = this
                }

                if (creature.isUnlocked && creature.currentNumberOfCoins < creature.maxNumberOfCoins) {
                    creature.timeSincePreviousGeneration += 1000

                    if (creature.timeSincePreviousGeneration >= creature.generationTime) {
                        generateCoins(viewHolder)
                    }

                    val timeToNextGenerationInSeconds = countTimeToNextGenerationInSeconds(
                        creature.generationTime,
                        creature.timeSincePreviousGeneration
                    )

                    setTimerText(timeToNextGenerationInSeconds, viewHolder)
                    creatureHandler.postDelayed(this, 1000)
                } else if (creature.currentNumberOfCoins == creature.maxNumberOfCoins) {
                    viewHolder.itemView.generation_timer.text = "Full"
                }
            }
        })

        viewHolder.itemView.apply {
            collect_button.setOnClickListener {
                if (creature.currentNumberOfCoins > 0) {
                    collectCoins(viewHolder, creatureHandlerRunner)
                }
            }

            unlock_button.setOnClickListener {
                if (user.totalNumberOfCoins >= creature.price) {
                    unlockCreature(viewHolder, creatureHandlerRunner)
                }
            }

            creature_image.setOnClickListener {
                showCreatureInfoDialog(viewHolder)
            }

            locked_creature_image.setOnClickListener {
                showCreatureInfoDialog(viewHolder)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun upgradeCreature(
        creature: Creature,
        upgradeCreatureButton: Button
    ) {
        user.totalNumberOfCoins -= creature.upgradePrice
        totalNumberOfCoinsTextView.text = "Coins: ${formatNumber(user.totalNumberOfCoins)}"
        creature.upgradePrice *= 2
        upgradeCreatureButton.text = formatNumber(creature.upgradePrice)
        stylizeUpgradeButton(upgradeCreatureButton)
        creature.level += 1
        when (creature.level) {
            2 -> {
                creature.coinsPerGeneration *= 2
            }
            3 -> {
                creature.maxNumberOfCoins *= 2
            }
            4 -> {
                creature.coinsPerGeneration = (creature.coinsPerGeneration * 1.5).toInt()
                creature.maxNumberOfCoins = (creature.maxNumberOfCoins * 1.5).toInt()
            }
            5 -> {
                creature.generationTime = (creature.generationTime / 1.33333333333).toLong()
                upgradeCreatureButton.visibility = View.GONE
            }
        }
        File("$filesDir/$userDataFileName").writeText(Gson().toJson(user))
    }

    private fun loadCreatureParameters(adapter: GroupAdapter<ViewHolder>, creature: Creature) {
        val coinsPerMinute =
            3600.toDouble() / (creature.generationTime / 1000) * creature.coinsPerGeneration

        adapter.add(
            CreatureParameterItem(
                R.drawable.ic_coin,
                "Coins at a time",
                creature.coinsPerGeneration,
                creature.level,
                creature.isUnlocked
            )
        )

        adapter.add(
            CreatureParameterItem(
                R.drawable.ic_time,
                "Generation time",
                creature.generationTime.toInt(),
                creature.level,
                creature.isUnlocked
            )
        )

        adapter.add(
            CreatureParameterItem(
                R.drawable.ic_storage,
                "Storage size",
                creature.maxNumberOfCoins,
                creature.level,
                creature.isUnlocked
            )
        )

        adapter.add(
            CreatureParameterItem(
                R.drawable.ic_bars,
                "Coins per hour",
                coinsPerMinute.toInt(),
                creature.level,
                creature.isUnlocked
            )
        )
    }

    @SuppressLint("SetTextI18n")
    private fun setTimerText(timeToNextGen: Int, viewHolder: ViewHolder) {
        viewHolder.itemView.generation_timer.apply {
            val hoursToNextGen = convertSecondsToHours(timeToNextGen)
            val minutesToNextGen = countTimeToNextGenerationInMinutes(timeToNextGen, hoursToNextGen)
            val secondsToNextGen = timeToNextGen - minutesToNextGen * 60

            when {
                hoursToNextGen == 0 && minutesToNextGen == 0 -> {
                    this.text = "${secondsToNextGen}s"
                }
                hoursToNextGen == 0 && secondsToNextGen == 0 -> {
                    this.text = "${minutesToNextGen}m"
                }
                hoursToNextGen == 0 && minutesToNextGen != 0 -> {
                    this.text = "${minutesToNextGen}m ${secondsToNextGen}s"
                }
                hoursToNextGen != 0 && minutesToNextGen == 0 -> {
                    this.text = "${hoursToNextGen}h"
                }
                hoursToNextGen != 0 && minutesToNextGen != 0 -> {
                    this.text = "${hoursToNextGen}h ${minutesToNextGen}m"
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun collectCoins(viewHolder: ViewHolder, creatureHandlerRunnable: Runnable?) {
        val numberOfCollectedCoins = creature.currentNumberOfCoins
        creature.currentNumberOfCoins = 0
        if (numberOfCollectedCoins == creature.maxNumberOfCoins) {
            creatureHandlerRunnable?.run()
        }
        viewHolder.itemView.number_of_coins.text =
            "${formatNumber(creature.currentNumberOfCoins)}/${formatNumber(creature.maxNumberOfCoins)}"
        stylizeCollectButton(viewHolder)
        if (user.totalNumberOfCoins < 1000000000) {
            user.totalNumberOfCoins += numberOfCollectedCoins
            totalNumberOfCoinsTextView.text = "Coins: ${formatNumber(user.totalNumberOfCoins)}"
        }
    }

    @SuppressLint("SetTextI18n")
    private fun generateCoins(viewHolder: ViewHolder) {
        creature.currentNumberOfCoins += creature.coinsPerGeneration
        creature.timeSincePreviousGeneration = 0
        viewHolder.itemView.number_of_coins.text =
            "${formatNumber(creature.currentNumberOfCoins)}/${formatNumber(creature.maxNumberOfCoins)}"
        stylizeCollectButton(viewHolder)
    }

    private fun stylizeLockedCreature(viewHolder: ViewHolder) {
        viewHolder.itemView.apply {
            creature_image.visibility = View.GONE
            locked_creature_image.visibility = View.VISIBLE
            unlock_button.visibility = View.VISIBLE
            price_text.visibility = View.VISIBLE
            price_text.text = formatNumber(creature.price)
            number_of_coins.visibility = View.GONE
            generation_timer.visibility = View.GONE
            collect_button.visibility = View.GONE
            timer_image.visibility = View.GONE
            stylizeUnlockButton(viewHolder)
        }
    }

    private fun stylizeUnlockButton(viewHolder: ViewHolder) {
        viewHolder.itemView.unlock_button.apply {
            if (user.totalNumberOfCoins >= creature.price) {
                setBackgroundResource(R.drawable.enabled_button_background)
                setTextColor(Color.WHITE)
                isClickable = true
            } else {
                setBackgroundResource(R.drawable.disabled_button_background)
                setTextColor(Color.BLACK)
                isClickable = false
            }
        }
    }

    private fun stylizeCollectButton(viewHolder: ViewHolder) {
        viewHolder.itemView.collect_button.apply {
            if (creature.currentNumberOfCoins > 0) {
                setBackgroundResource(R.drawable.enabled_button_background)
                setTextColor(Color.WHITE)
                isClickable = true
            } else {
                setBackgroundResource(R.drawable.disabled_button_background)
                setTextColor(Color.BLACK)
                isClickable = false
            }
        }
    }

    private fun stylizeUpgradeButton(upgradeButton: Button) {
        upgradeButton.apply {
            if (user.totalNumberOfCoins >= creature.upgradePrice) {
                setBackgroundResource(R.drawable.enabled_button_background)
                setTextColor(Color.WHITE)
                isClickable = true
            } else {
                setBackgroundResource(R.drawable.disabled_button_background)
                setTextColor(Color.BLACK)
                isClickable = false
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun unlockCreature(viewHolder: ViewHolder, creatureHandlerRunnable: Runnable?) {
        user.totalNumberOfCoins -= creature.price
        creature.isUnlocked = true
        creatureHandlerRunnable?.run()
        totalNumberOfCoinsTextView.text = "Coins: ${formatNumber(user.totalNumberOfCoins)}"
        viewHolder.itemView.apply {
            creature_image.visibility = View.VISIBLE
            creature_image.setImageResource(creature.firstLevelImageId)
            locked_creature_image.visibility = View.GONE
            number_of_coins.text =
                "${formatNumber(creature.currentNumberOfCoins)}/${formatNumber(creature.maxNumberOfCoins)}"
            unlock_button.visibility = View.GONE
            number_of_coins.visibility = View.VISIBLE
            generation_timer.visibility = View.VISIBLE
            collect_button.visibility = View.VISIBLE
            timer_image.visibility = View.VISIBLE
            price_text.visibility = View.GONE
        }
    }

    private fun countCoinsGeneratedWhileUserWasAbsent() {
        if (user.lastTimePresent != 0L) {
            val currentTime = Calendar.getInstance().time.time
            val timeUserWasAbsent = currentTime - user.lastTimePresent

            val numberOfGenerationsMissed =
                countNumberOfGenerationsMissed(
                    creature.timeSincePreviousGeneration,
                    timeUserWasAbsent,
                    creature.generationTime
                )

            val numberOfCoinsGeneratedWhileUserWasAbsent =
                numberOfGenerationsMissed * creature.coinsPerGeneration

            creature.currentNumberOfCoins += numberOfCoinsGeneratedWhileUserWasAbsent

            if (creature.currentNumberOfCoins > creature.maxNumberOfCoins) {
                creature.currentNumberOfCoins = creature.maxNumberOfCoins
            }

            if (creature.currentNumberOfCoins < creature.maxNumberOfCoins) {
                creature.timeSincePreviousGeneration = countTimeSincePreviousGeneration(
                    creature.timeSincePreviousGeneration,
                    timeUserWasAbsent,
                    creature.generationTime,
                    numberOfGenerationsMissed
                )
            } else {
                creature.timeSincePreviousGeneration = 0
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showCreatureInfoDialog(viewHolder: ViewHolder) {
        val creatureDialog = Dialog(mainActivityContext)
        creatureDialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(false)
            setContentView(R.layout.creature_dialog)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            creature_name_text.text = creature.name
            val creatureParametersAdapter = GroupAdapter<ViewHolder>()
            creature_parameters_recycler_view.adapter = creatureParametersAdapter
            loadCreatureParameters(creatureParametersAdapter, creature)

            if (creature.isUnlocked) {
                if (creature.level < 5) {
                    creature_level_text.text = "Level ${creature.level}"
                    creature_avatar_image.setImageResource(creature.firstLevelImageId)
                    upgrade_creature_button.text = formatNumber(creature.upgradePrice)
                    stylizeUpgradeButton(upgrade_creature_button)
                } else {
                    creature_level_text.text = "Max level"
                    creature_avatar_image.setImageResource(creature.fifthLevelImageId)
                    upgrade_creature_button.visibility = View.GONE
                }
            } else {
                creature_level_text.text = "Level 1"
                creature_avatar_image.setImageResource(creature.previewImageId)
                upgrade_creature_button.visibility = View.GONE
            }

            upgrade_creature_button.setOnClickListener {
                if (user.totalNumberOfCoins >= creature.upgradePrice && creature.level < 5) {
                    upgradeCreature(
                        creature,
                        upgrade_creature_button
                    )
                    creatureParametersAdapter.clear()
                    loadCreatureParameters(creatureParametersAdapter, creature)
                    viewHolder.itemView.number_of_coins.text =
                        "${formatNumber(creature.currentNumberOfCoins)}/${
                            formatNumber(
                                creature.maxNumberOfCoins
                            )
                        }"
                    if (creature.level < 5) {
                        creature_level_text.text = "Level ${creature.level}"
                    } else {
                        creature_level_text.text = "Max level"
                        creature_avatar_image.setImageResource(creature.fifthLevelImageId)
                        viewHolder.itemView.creature_image.setImageResource(creature.fifthLevelImageId)
                    }
                }
            }

            close_image.setOnClickListener {
                dismiss()
            }

            show()
        }
    }
}
