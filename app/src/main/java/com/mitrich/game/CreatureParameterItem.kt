package com.mitrich.game

import android.view.View
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.creature_parameter_item.view.*

class CreatureParameterItem(
    private val parameterImageId: Int,
    private val parameterName: String,
    private var parameterValue: Int,
    private val creatureLevel: Int,
    private val creatureIsUnlocked: Boolean
) : Item<ViewHolder>() {

    override fun getLayout() = R.layout.creature_parameter_item

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.apply {
            parameter_image.setImageResource(parameterImageId)
            parameter_name_text.text = parameterName

            when (parameterName) {
                "Generation time" -> {
                    val generationTime = parameterValue / 1000
                    val generationHours = convertSecondsToHours(generationTime)
                    val generationMinutes =
                        countTimeToNextGenerationInMinutes(generationTime, generationHours)
                    val generationSeconds = generationTime - generationMinutes * 60

                    when {
                        generationHours == 0 && generationMinutes == 0 -> {
                            parameter_value_text.text = "${generationSeconds}s"
                        }
                        generationHours == 0 && generationSeconds == 0 -> {
                            parameter_value_text.text = "${generationMinutes}m"
                        }
                        generationHours == 0 && generationMinutes != 0 -> {
                            parameter_value_text.text =
                                "${generationMinutes}m ${generationSeconds}s"
                        }
                        generationHours != 0 && generationMinutes == 0 -> {
                            parameter_value_text.text = "${generationHours}h"
                        }
                        generationHours != 0 && generationMinutes != 0 -> {
                            parameter_value_text.text = "${generationHours}h ${generationMinutes}m"
                        }
                    }
                }
                else -> {
                    parameter_value_text.text = formatNumber(parameterValue)
                }
            }

            if (creatureIsUnlocked) {
                when (parameterName) {
                    "Coins at a time" -> {
                        if (creatureLevel == 1 || creatureLevel == 3) {
                            progress_image.setImageResource(R.drawable.ic_arrows_up)
                            progress_image.visibility = View.VISIBLE
                        } else {
                            progress_image.visibility = View.GONE
                        }
                    }
                    "Generation time" -> {
                        if (creatureLevel == 4) {
                            progress_image.setImageResource(R.drawable.ic_arrows_down)
                            progress_image.visibility = View.VISIBLE
                        } else {
                            progress_image.visibility = View.GONE
                        }
                    }
                    "Storage size" -> {
                        if (creatureLevel == 2 || creatureLevel == 3) {
                            progress_image.setImageResource(R.drawable.ic_arrows_up)
                            progress_image.visibility = View.VISIBLE
                        } else {
                            progress_image.visibility = View.GONE
                        }
                    }
                    "Coins per hour" -> {
                        if (creatureLevel != 2 && creatureLevel != 5) {
                            progress_image.setImageResource(R.drawable.ic_arrows_up)
                            progress_image.visibility = View.VISIBLE
                        } else {
                            progress_image.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }
}
